﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAroundPoint : MonoBehaviour {

    public float distance = 10.0f;
    public float rotateSpeed = 36.0f;
    public float moveSpeed = 2.0f;

	private bool doRotate = false;

    // Use this for initialization
    void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        Vector3 centerPoint = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        // ct = Camera.main.transform;

        Vector3 mp = Input.mousePosition;
        // mp.z = distance;
        mp = Camera.main.ScreenToWorldPoint(mp);


		if (Input.GetKeyDown(KeyCode.RightShift))
        {
			doRotate = true;
        }
		if (Input.GetKeyUp(KeyCode.RightShift))
		{
			doRotate = false;
		}

		if (Input.GetMouseButton (0)) {
			centerPoint += transform.forward * distance;
		}

		var rot = doRotate || Input.GetMouseButton(0);
        //Debug.Log(centerPoint);

        var hMove = Input.GetAxis("horizontal");
        var vMove = Input.GetAxis("vertical");
        // transform.RotateAround(centerPoint, speed * Time.deltaTime);
        // transform.Rotate(Vector3.up, hMove * rotateSpeed * Time.deltaTime, Space.World);
        //transform.RotateAround(centerPoint, Vector3.up, hMove * rotateSpeed * Time.deltaTime);
        transform.Translate(Vector3.forward * vMove * moveSpeed * Time.deltaTime);
		if (rot) {
			transform.RotateAround (centerPoint, Vector3.up, hMove * rotateSpeed * Time.deltaTime);	
		} else {
			transform.Translate(Vector3.right * hMove * moveSpeed * Time.deltaTime);
		}

    }
}
