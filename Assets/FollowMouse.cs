﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowMouse : MonoBehaviour {

    /**
     * Units per second 
     */
    public float moveSpeed;

    public float distance;

    private Transform t;

	// Use this for initialization
	void Start () {
        // the current game object's transform
        t = GetComponent<Transform>();
    }
	
	// Update is called once per frame
	void Update () {

        Vector3 mp = Input.mousePosition;
        mp.z = distance;
        mp = Camera.main.ScreenToWorldPoint(mp);
        // Debug.Log(mp);

        t.position = Vector3.Lerp(t.position, mp, moveSpeed * Time.deltaTime);
        // position = mp;
    }
}
