﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtlasTexture : MonoBehaviour {

	public int textureIndex = 0;
	private int _prevTextureIndex = -1;

	private float offset;
	private Mesh m;
	private Vector2[] uvs;
	public Vector2 atlasGrid = new Vector2(8, 8);

	void Start () {
		_prevTextureIndex = -1;

		m = GetComponent<MeshFilter>().mesh;
		uvs = m.uv;
		shiftUVs (textureIndex);
	}
	
	void Update () {
		if (_prevTextureIndex != textureIndex) {
			_prevTextureIndex = textureIndex;
			shiftUVs (textureIndex);
		}
	}

	private void shiftUVs(int index)
	{
		Vector2[] nuvs = new Vector2[uvs.Length];
		for (int i = 0; i < uvs.Length; i++) 
		{
			nuvs[i].x = (uvs[i].x + (index % atlasGrid.x)) / atlasGrid.x;
			nuvs[i].y = (uvs[i].y + Mathf.Floor(index/atlasGrid.y)) / atlasGrid.y;
		}
		m.uv = nuvs;
	}
}
