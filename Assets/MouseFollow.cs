﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseFollow : MonoBehaviour {

	public float distanceToCamera = 5.0f;
	/**
	 * how fast should the object follow the mouse (pixels per frame?)
	 */
	public float followSpeed = 3.0f;

	private Transform trans;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		var mousePos = Input.mousePosition;
		var wantedPos = Camera.main.ScreenToWorldPoint (new Vector3(mousePos.x, mousePos.y, distanceToCamera));
		// transform.position = wantedPos;
		transform.position = Vector3.Lerp(transform.position, wantedPos, Time.deltaTime * followSpeed);
	}
}
