﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;
using data;

[RequireComponent(typeof(SoundController))]
[RequireComponent(typeof(MouseInstantiator))]
public class MobileGame : MonoBehaviour {

	public GameObject[] cubes;
    public Canvas mainCanvas;

    private CamMouseLook cml;
	private SoundConfig config;
	private SoundController soundController;
	private MouseInstantiator mouseInstantiator;
    private bool cursorVisible;
    private SoundData currentPick;
    private int strike;
    private float timeSinceLastInput;

    public GameModeType gameModeType;

	// Use this for initialization
	void Start()
	{
        cml = Camera.main.GetComponent<CamMouseLook>();
        cml.enabled = false;
        mainCanvas.enabled = true;

        soundController = GetComponent<SoundController>();
		mouseInstantiator = GetComponent<MouseInstantiator>();
		loadXmlData();
        setCursorVisible(true);
    }

    public void startGame(int gameMode)
    {
        gameModeType = (GameModeType) gameMode;

        mainCanvas.enabled = false;
        cml.enabled = true;
        setCursorVisible(false);

        strike = 0;
        timeSinceLastInput = 0;

        if (gameModeType == GameModeType.Testing)
        {
            playSounds(doPick());
        }

    }

    private void playSounds(List<SoundData> soundsToPlay)
    {
        StartCoroutine(playConcatSounds(soundsToPlay));
    }
   
    private List<SoundData> doPick()
    {
        List<SoundData> soundsToPlay = new List<SoundData>();
        soundsToPlay.Add(config.rndSound(SoundType.Start));
        currentPick = config.rndSound(SoundType.Char);
        soundsToPlay.Add(currentPick);
        return soundsToPlay;
    }
	
	// Update is called once per frame
	void Update()
	{
        if (!mainCanvas.enabled)
        {
            switch (gameModeType)
            {
                case GameModeType.Learning:
                    updateLearning();
                    break;

                case GameModeType.Testing:
                    updateTesting();
                    break;

                default:
                    break;
            }
        }
        
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            mainCanvas.enabled = !mainCanvas.enabled;
            cml.enabled = !cml.enabled;
            setCursorVisible(!cursorVisible);
        }
    }

    private void updateLearning()
    {
        var p = getPressedSound();
        if (p != null)
        {
            mouseInstantiator.createCube(p);
            soundController.playSound(config.getPath(p));
        }
    }

    private SoundData getPressedSound()
    {
        for (int c = 0; c < config.charSounds.Count; c++)
        {
            SoundData sd = config.charSounds[c];
            if (Input.GetKeyDown(sd.character))
            {
                return sd;
            }
        }
        return null;
    }

    private void updateTesting()
    {
        List<SoundData> soundsToPlay = new List<SoundData>();
        bool correct = false;
        if (Input.anyKeyDown)
        {
            if (Input.GetKeyDown(currentPick.character))
            {
                mouseInstantiator.createCube(currentPick);
                soundsToPlay.Add(config.rndSound(SoundType.Good));
                if (strike < 0) strike = 0;
                strike++;
                correct = true;
            }
            else
            {
                if (
                    Input.GetAxis("vertical") == 0 &&
                    Input.GetAxis("horizontal") == 0 &&
                    !Input.GetButtonDown("fire") && !Input.GetButtonDown("jump") && !Input.GetKeyDown(KeyCode.Escape)
                    )
                {

                    var p = getPressedSound();
                    if (p != null)
                    {
                        soundsToPlay.Add(p);
                    }

                    soundsToPlay.Add(config.rndSound(SoundType.Bad));
                    if (strike > 0) strike = 0;
                    strike--;
                    
                }
                
            }
            timeSinceLastInput = 0;
        }
        else
        {
            timeSinceLastInput += Time.deltaTime;
        }

        if (strike == 3)
        {
            soundsToPlay.Add(config.rndSound(SoundType.Perfect));
            strike = 0;
        }

        if (strike == -3 || timeSinceLastInput > 5)
        {
            soundsToPlay.Add(config.rndSound(SoundType.Wait));
            soundsToPlay.Add(currentPick);
            strike = 0;
            timeSinceLastInput = 0;
        }

        if (correct)
        {
            List<SoundData> newSounds = doPick();
            foreach (SoundData snd in newSounds)
            {
                soundsToPlay.Add(snd);
            }
        }

        playSounds(soundsToPlay);

    }

	private void assignRandomPick()
	{
		List<SoundData> picks = new List<SoundData>();
		while (picks.Count < 6) {
			SoundData pickedChar = config.charSounds[Random.Range (0, config.charSounds.Count)];
			if (picks.IndexOf(pickedChar) < 0) {
				picks.Add(pickedChar);
			}
		}

		for (int i = 0; i < picks.Count; i++) {
			AtlasTexture at = cubes[i].GetComponent<AtlasTexture>();
			if (at != null) {
				at.textureIndex = picks[i].index;
			} else {
				Debug.LogError ("Not enough cubes to display picks");
			}
		}

		List<SoundData> soundsToPlay = new List<SoundData>();
		soundsToPlay.Add(config.rndSound(SoundType.Start));
        soundsToPlay.Add(config.rndSound(picks));

		StartCoroutine(playConcatSounds(soundsToPlay));
	}

	IEnumerator playConcatSounds(List<SoundData> list)
	{
		Debug.Log ("should play: " + list);
		for (int i = 0; i < list.Count; i++) {
			AudioClip ac = soundController.playSound(config.getPath(list [i]));
			Debug.Log ("should play: " + list[i]);
			yield return new WaitForSeconds(ac.length * 0.95f);
		}
	}

	private void loadXmlData()
	{
		var xml = Resources.Load<TextAsset>("xml/sounds");

		using(var reader = new StringReader(xml.text))
		{
			var serializer = new XmlSerializer(typeof(SoundConfig));
			config = serializer.Deserialize(reader) as SoundConfig;
            config.initialize();
		}
	}

    private void setCursorVisible(bool visible)
    {
        cursorVisible = visible;
        if (visible)
        {
            Cursor.lockState = CursorLockMode.None;
        }
        else
        {
            Cursor.lockState = CursorLockMode.Locked;
        }
        Debug.Log("Cursor visible " + cursorVisible);
        Cursor.visible = cursorVisible;
    }
}

public enum GameModeType { Learning, Testing };