﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSController : MonoBehaviour {

	public float moveSpeed;
	public float rotateSpeed;

	private Rigidbody body;
	

	// Use this for initialization
	void Start () {
		
		body = gameObject.GetComponent<Rigidbody> ();
	}


	// Update is called once per frame
	void Update () {
		var hMove = Input.GetAxis("horizontal");
		var vMove = Input.GetAxis("vertical");
		transform.Translate(hMove * moveSpeed * Time.deltaTime, 0f, vMove * moveSpeed * Time.deltaTime);

		

		if (Input.GetButtonDown("jump")) {
			// Debug.Log ("JUMP");
			// Debug.Log (body);
			// body.AddForce (Vector3.up * 500f);
			var vel = body.velocity;
			vel.y = 10f;
			body.velocity = vel;


		}
	}
	
}
