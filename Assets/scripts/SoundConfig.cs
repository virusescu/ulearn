﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;
using System.IO;

namespace data
{
    public enum SoundType
    {
        Char,
        Start,
        Good,
        Bad,
        Wait,
        Perfect
    }

	[XmlRoot("soundConfig")]
	public class SoundConfig
	{

        // auto-set from xml node
        public string basePath;

        [XmlArray("chars")]
		[XmlArrayItem("soundData")]
		public List<SoundData> charSounds = new List<SoundData>();

		[XmlArray("start")]
		[XmlArrayItem("soundData")]
		public List<SoundData> startSounds = new List<SoundData>();

		[XmlArray("good")]
		[XmlArrayItem("soundData")]
		public List<SoundData> goodSounds = new List<SoundData>();

		[XmlArray("bad")]
		[XmlArrayItem("soundData")]
		public List<SoundData> badSounds = new List<SoundData>();

		[XmlArray("wait")]
		[XmlArrayItem("soundData")]
		public List<SoundData> waitSounds = new List<SoundData>();

		[XmlArray("perfect")]
		[XmlArrayItem("soundData")]
		public List<SoundData> perfectSounds = new List<SoundData>();

        private Dictionary<SoundType, List<SoundData>> map;

		public SoundConfig () { }

        public void initialize()
        {
            map = new Dictionary<SoundType, List<SoundData>>();
            map.Add(SoundType.Char, charSounds);
            map.Add(SoundType.Start, startSounds);
            map.Add(SoundType.Good, goodSounds);
            map.Add(SoundType.Bad, badSounds);
            map.Add(SoundType.Wait, waitSounds);
            map.Add(SoundType.Perfect, perfectSounds);

            setBasePath(startSounds, "word");
            setBasePath(goodSounds, "word");
            setBasePath(badSounds, "word");
            setBasePath(waitSounds, "word");
            setBasePath(perfectSounds, "word");
        }

        private void setBasePath(List<SoundData> list, string bp)
        {
            foreach (SoundData sd in list)
            {
                sd.basePath = bp;
            }
        }

		public string getPath(SoundData sd)
		{
			return basePath + sd.basePath + "-" + sd.character + "-" + Random.Range(1, sd.variants + 1);
		}

        /**
         * Returns a random sound of type t
         */
        public SoundData rndSound(SoundType t)
        {
           return rndPick(map[t]);
        }

        /**
         * Returns a random sound path from a list of SoundData sounds 
         */
        public SoundData rndSound(List<SoundData> sounds)
        {
            return rndPick(sounds);
        }


        private SoundData rndPick(List<SoundData> list)
		{
			return list[Random.Range (0, list.Count)];
		}
	}
}

