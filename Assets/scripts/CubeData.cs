﻿using UnityEngine;

public class CubeData {

	public int index;
	public KeyCode key;

	public CubeData(int index, KeyCode key)
	{
		this.index = index;
		this.key = key;
		// TODO - maybe add details about the audio path
		// or just add a getter for the path to be constructed here
	}

	public CubeData(int index)
	{
		this.index = index;
	}

}