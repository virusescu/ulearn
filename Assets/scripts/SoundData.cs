﻿using System.Xml;
using System.Xml.Serialization;

namespace data
{
	public class SoundData
	{
		[XmlAttribute("char")]
		public string character;

		[XmlAttribute("index")]
		public int index;

		[XmlAttribute("variants")]
		public int variants = 1;

        public string basePath = "char";

		public SoundData ()
		{
		}
	}
}

