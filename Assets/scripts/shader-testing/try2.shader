// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// derived from - Unity Answers Diffuse Rim
Shader "-OO/Try2"
 {
     Properties
     {
         _MainTex        ("Base (RGB)",      2D)                    = "white" { }
         _Color          ("Main Color",      Color)                 = (.5,.5,.5,1)
         _OutlineColor   ("Outline Color",   Color)                 = (0,0,0,1)
         _OutlineOffset  ("Outline offset",  Range (-1, 1))          = 0.1
         _OutlineSize    ("Outline size",    Range (0, 1))          = 0.1
     }
 SubShader
 {
     Tags { "RenderType"="Opaque" }
     CGPROGRAM
     #pragma surface surf Lambert
     sampler2D _MainTex;
     fixed4 _Color;
     struct Input
     {
         float2 uv_MainTex;
     };
     void surf (Input IN, inout SurfaceOutput o)
     {
         fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
         o.Albedo = c.rgb;
         o.Alpha = c.a;
     }
     ENDCG
     //ouline pass
     Pass
     {
         // Blend SrcAlpha OneMinusSrcAlpha
		 // BlendOp LogicalInvert
		 // BlendOp LogicalAnd
		 // BlendOp LogicalNand
		 // BlendOp LogicalOr
		 // BlendOp LogicalNor
		 BlendOp LogicalXor
		 // Blend One One // Additive
         // Cull Back
         Offset -1, -1
         Lighting Off
         //ZWrite Off
		 ZTest Always
     
         CGPROGRAM
         #pragma vertex vert
         #pragma fragment frag
         float _OutlineOffset;
         float _OutlineSize;
         fixed4 _OutlineColor;
         
		 struct appdata
         {
            float4 vertex : POSITION;
            float3 normal : NORMAL;
         };
		 
         struct v2f
         {
             float4 pos : SV_POSITION; // project view position
             float4 wPos : TEXCOORD0; // world-based position
             float4 wNor : TEXCOORD1; // world-based normal
         };
		 
         v2f vert(appdata v)
         {
             v2f o;
             v.vertex.xyz += _OutlineOffset * v.normal;
             o.pos = UnityObjectToClipPos(v.vertex);
             o.wPos = mul(unity_ObjectToWorld, v.vertex);
             o.wNor = mul(unity_ObjectToWorld, float4(v.normal.xyz, 0));
             return o;
         }
         half4 frag(v2f i) : COLOR
         {
             float3 worldNormal = normalize(i.wNor.xyz);
             float3 worldToCameraDirection = normalize(_WorldSpaceCameraPos - i.wPos.xyz);
             half4 answer = _OutlineColor;
             //answer.a *= saturate((_OutlineSize - dot(worldNormal, worldToCameraDirection)) * 100.0);
             // answer.a *= _OutlineSize - dot(worldNormal, worldToCameraDirection);
             answer.a *= clamp((0.5 - dot(worldNormal, worldToCameraDirection)) * 300, 0, 1);
             return answer;
         }
         ENDCG
     }
	 
 }
 }