// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "-OO/Personal-try1"
{
	Properties {
		_BaseColor ("Base Color", Color) = (1,1,1,1)
		_GlowColor ("Glow Color", Color) = (1,1,1,1)
		_MainTex ("Texture", 2D) = "white" {}
		_Normal ("Normal Map", 2D) = "bump" {}
		_Amount ("Extrusion Amount", Range(0,3)) = 0
	}
	
	SubShader {
		
		Tags { "Queue" = "Geometry" }

		Pass {
		
			Tags { 
				"Queue" = "Opaque"
				"RenderType" = "Transparent" 
			}

			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag

			struct VertInput {
				float4 pos 		: POSITION;
				float2 uv 		: TEXCOORD0;
			};  

			struct VertOutput {
				float4 pos 		: SV_POSITION;
				float2 uv 		: TEXCOORD0;
			};
			
			sampler2D _MainTex;
			half4 _BaseColor;

			VertOutput vert(VertInput vi) {
				VertOutput o;
				o.uv = vi.uv;
				o.pos = UnityObjectToClipPos(vi.pos);
				return o;
			}

			half4 frag(VertOutput vertx) : COLOR {
				half4 t = tex2D(_MainTex, vertx.uv) * _BaseColor;
				return t;
			}
			
			ENDCG
		}
		
		Pass {
		
			Tags { "LightMode" = "Always" }
			Tags { 
				"Queue" = "Geometry"
				"RenderType" = "Transparent" 
			}
			
			// Cull Off
			ZWrite Off
			ZTest Always
			ColorMask RGB // alpha not used
 
			// you can choose what kind of blending mode you want for the outline
			Blend SrcAlpha OneMinusSrcAlpha // Normal
			// Blend One One // Additive
			// Blend One OneMinusDstColor // Soft Additive
			// Blend DstColor Zero // Multiplicative
			// Blend DstColor SrcColor // 2x Multiplicative
			
			CGPROGRAM
			
			#pragma vertex vert
			#pragma fragment frag

			struct VertInput {
				float4 pos 		: POSITION;
				float2 uv 		: TEXCOORD0;
				float3 normal	: NORMAL;
			};  

			struct VertOutput {
				float4 pos 		: SV_POSITION;
				float2 uv 		: TEXCOORD0;
				float3 normal 	: NORMAL;
			};
			
			sampler2D _MainTex;
			sampler2D _NormalMap;
			float _Amount;
			half4 _BaseColor;
			half4 _GlowColor;

			VertOutput vert(VertInput vi) {
				VertOutput o;
				o.uv = vi.uv;
				o.normal = vi.normal;
				vi.pos.xyz += vi.normal * _Amount;
				o.pos = UnityObjectToClipPos(vi.pos);
				
				return o;
			}

			half4 frag(VertOutput vertx) : COLOR {
				float4 t = tex2D(_MainTex, vertx.uv);
				// return t.r;
				// return half4(1, 1, 1, 0.0);
				return half4(_GlowColor.rgb, step(t, _BaseColor).r);
				// return step(t, _BaseColor).r;
			}
			
			ENDCG
		}
		
		
		
	}
}