﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class ClickDestroy : MonoBehaviour {

	private bool destroying;
	private AudioSource audioSource;

	public AudioClip[] bounceSounds;
	public AudioClip[] destroySounds;

    public GameObject[] explosions;

	// Use this for initialization
	void Start () {
		destroying = false;
		audioSource = GetComponent<AudioSource> ();
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown(0) && !destroying){
			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			if (Physics.Raycast (ray, out hit)){
				Debug.DrawLine (ray.origin, hit.point);
				if (hit.transform == gameObject.transform) 
				{
					var di = (int) Mathf.Floor (Random.Range (0.0f, 1.0f) * 2.0f);
					// Debug.Log (di);
					audioSource.clip = destroySounds[di];
					// audioSource.PlayOneShot (destroySounds [di], 0.3f);
					audioSource.Play(); // delay can be speciffied
					destroying = true;
                    var de = (int)Mathf.Floor(Random.Range(0.0f, 1.0f) * 6.0f);
                    Instantiate(explosions[de], transform.position, Quaternion.identity);
                }
			}
		}
		if (destroying) {
			gameObject.transform.localScale = gameObject.transform.localScale * 0.4f;
			if (gameObject.transform.localScale.x < 0.05f) {
				Destroy (gameObject);
			}
		}
	}

	void OnCollisionEnter()
	{
		audioSource.clip = bounceSounds [(int)Mathf.Floor (Random.Range (0.0f, 1.0f) * 2.0f)];
		audioSource.Play();
	}
}
