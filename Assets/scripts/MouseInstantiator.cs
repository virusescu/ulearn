﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using data;

public class MouseInstantiator : MonoBehaviour {

	public GameObject what;
	public GameObject pointer;

	public void createCube(SoundData charData)
	{
		Vector3 position = pointer.transform.position;
		position.y = Mathf.Max (2f, position.y);

		var cube = Instantiate (what, position, Quaternion.identity) as GameObject;
		var atlasTexture = cube.GetComponent<AtlasTexture> ();

		cube.transform.LookAt (Camera.main.transform, Vector3.down);

		atlasTexture.textureIndex = charData.index;
	}
}
