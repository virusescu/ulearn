﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;
using System.IO;
using data;

public class SoundController : MonoBehaviour {

	[SerializeField]
    private AudioSource[] channels;

	private int currentChannel = 0;
	private Dictionary<string, AudioClip> clips;

	void Start () {
		clips = new Dictionary<string, AudioClip>();
    }

	public AudioClip playSound(string path)
	{
		if (!clips.ContainsKey (path)) {
			var r = Resources.Load (path, typeof(AudioClip));
			clips.Add(path, (AudioClip) r);
		}

		var channel = channels[currentChannel++];
		if (currentChannel >= channels.Length) {
			currentChannel = 0;
		}
		var c = clips[path];
		channel.clip = c;
		channel.volume = 1.0f;
		channel.Play();
		return c;
	}
}
