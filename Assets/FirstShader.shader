﻿Shader "Custom/FirstShader" {

	Properties{
		_myDiffuse("Diffuse Texture", 2D) = "white" {}
		_myBrightness("Brightness", Range(0, 4)) = 1

		_SpecColor("Specular Colour", Color) = (1, 1, 1, 1)
		_specAmmount("Specular Ammount", Range(0, 2)) = 0.5
		_glossAmmount("Glossiness Ammount", Range(0, 2)) = 0.5

		_myNormal("Normal", 2D) = "White" {}
		_myNormalAmmount("Bump Intensity", Range(0, 2)) = 1

		_myReflections("Reflections", CUBE) = "" {}
		_myReflectionsIntensity("Reflections Intensity", Range(0, 2)) = 1

		_myDisolveMap("Disolve Map", 2D) = "white" {}
		_myDisolveAmmount("Disolve Ammount", Range(0, 1)) = 0
	}
	SubShader {

		//Tags { "Queue"="Transparent" "RenderType"="Transparent" }

		// ZWrite On
		// Blend SrcAlpha OneMinusSrcAlpha

		CGPROGRAM
			//#pragma surface surf Lambert alpha
			#pragma surface surf BlinnPhong

			sampler2D 	_myDiffuse;
			sampler2D 	_myNormal;
			half		_specAmmount;
			fixed		_glossAmmount;

			samplerCUBE _myReflections;
			half		_myReflectionsIntensity;

			half		_myNormalAmmount;
			half		_myBrightness;

			sampler2D 	_myDisolveMap;
			half 		_myDisolveAmmount;

			struct Input {
				float2 uv_myDiffuse;
				float2 uv_myNormal;
				float2 uv_myDisolveMap;
				float3 worldRefl; INTERNAL_DATA 
			};


			void surf(Input i, inout SurfaceOutput o) {
				o.Albedo = (tex2D(_myDiffuse, i.uv_myDiffuse) * _myBrightness).rgb;
				o.Albedo = clamp(o.Albedo, 0, 1);

				//o.Alpha  = (tex2D(_myDisolveMap, i.uv_myDisolveMap) * (_myDisolveAmmount)).r;
				o.Alpha = 1;

				o.Normal = UnpackNormal(tex2D(_myNormal, i.uv_myNormal));
				o.Normal *= float3(_myNormalAmmount, _myNormalAmmount, 1);


				// o.Emission = texCUBE(_myReflections, i.worldRefl).rgb;
		        o.Emission = (texCUBE (_myReflections, WorldReflectionVector (i, o.Normal)) * _myReflectionsIntensity).rgb;
		        //o.Albedo = (texCUBE (_myReflections, WorldReflectionVector (i, o.Normal)) * _myReflectionsIntensity).rgb;

		        o.Specular = _specAmmount;
		        o.Gloss = _glossAmmount;
			}
		ENDCG
	}
	FallBack "Diffuse"
}
