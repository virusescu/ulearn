Sound pack downloaded from Freesound
----------------------------------------

This pack of sounds contains sounds by the following user:
 - Corsica_S ( https://freesound.org/people/Corsica_S/ )

You can find this pack online at: https://freesound.org/people/Corsica_S/packs/6960/

License details
---------------

Attribution: http://creativecommons.org/licenses/by/3.0/


Sounds in this pack
-------------------

  * 109940__Corsica_S__zucchini.wav
    * url: https://freesound.org/s/109940/
    * license: Attribution
  * 109939__Corsica_S__zero2.wav
    * url: https://freesound.org/s/109939/
    * license: Attribution
  * 109938__Corsica_S__zebra.wav
    * url: https://freesound.org/s/109938/
    * license: Attribution
  * 109937__Corsica_S__yarn.wav
    * url: https://freesound.org/s/109937/
    * license: Attribution
  * 109936__Corsica_S__yak.wav
    * url: https://freesound.org/s/109936/
    * license: Attribution
  * 109935__Corsica_S__yacht.wav
    * url: https://freesound.org/s/109935/
    * license: Attribution
  * 109934__Corsica_S__xylophone.wav
    * url: https://freesound.org/s/109934/
    * license: Attribution
  * 109933__Corsica_S__xiphias.wav
    * url: https://freesound.org/s/109933/
    * license: Attribution
  * 109932__Corsica_S__xebec.wav
    * url: https://freesound.org/s/109932/
    * license: Attribution
  * 109931__Corsica_S__windows.wav
    * url: https://freesound.org/s/109931/
    * license: Attribution
  * 109930__Corsica_S__wallet.wav
    * url: https://freesound.org/s/109930/
    * license: Attribution
  * 109929__Corsica_S__waffle.wav
    * url: https://freesound.org/s/109929/
    * license: Attribution
  * 109928__Corsica_S__volcano.wav
    * url: https://freesound.org/s/109928/
    * license: Attribution
  * 109927__Corsica_S__violin.wav
    * url: https://freesound.org/s/109927/
    * license: Attribution
  * 109926__Corsica_S__video_camera.wav
    * url: https://freesound.org/s/109926/
    * license: Attribution
  * 109925__Corsica_S__union.wav
    * url: https://freesound.org/s/109925/
    * license: Attribution
  * 109924__Corsica_S__uniform.wav
    * url: https://freesound.org/s/109924/
    * license: Attribution
  * 109923__Corsica_S__umbrella.wav
    * url: https://freesound.org/s/109923/
    * license: Attribution
  * 109922__Corsica_S__train.wav
    * url: https://freesound.org/s/109922/
    * license: Attribution
  * 109921__Corsica_S__tomato.wav
    * url: https://freesound.org/s/109921/
    * license: Attribution
  * 109920__Corsica_S__tiger.wav
    * url: https://freesound.org/s/109920/
    * license: Attribution
  * 109919__Corsica_S__star.wav
    * url: https://freesound.org/s/109919/
    * license: Attribution
  * 109918__Corsica_S__spoon.wav
    * url: https://freesound.org/s/109918/
    * license: Attribution
  * 109917__Corsica_S__skateboard.wav
    * url: https://freesound.org/s/109917/
    * license: Attribution
  * 109916__Corsica_S__rhinoceros.wav
    * url: https://freesound.org/s/109916/
    * license: Attribution
  * 109915__Corsica_S__rain.wav
    * url: https://freesound.org/s/109915/
    * license: Attribution
  * 109914__Corsica_S__rabbit.wav
    * url: https://freesound.org/s/109914/
    * license: Attribution
  * 109913__Corsica_S__question_mark.wav
    * url: https://freesound.org/s/109913/
    * license: Attribution
  * 109912__Corsica_S__queen.wav
    * url: https://freesound.org/s/109912/
    * license: Attribution
  * 109911__Corsica_S__quartz.wav
    * url: https://freesound.org/s/109911/
    * license: Attribution
  * 109910__Corsica_S__pig.wav
    * url: https://freesound.org/s/109910/
    * license: Attribution
  * 109909__Corsica_S__pen.wav
    * url: https://freesound.org/s/109909/
    * license: Attribution
  * 109908__Corsica_S__paint.wav
    * url: https://freesound.org/s/109908/
    * license: Attribution
  * 109907__Corsica_S__owl.wav
    * url: https://freesound.org/s/109907/
    * license: Attribution
  * 109906__Corsica_S__orange.wav
    * url: https://freesound.org/s/109906/
    * license: Attribution
  * 109905__Corsica_S__oars.wav
    * url: https://freesound.org/s/109905/
    * license: Attribution
  * 109904__Corsica_S__nutshell.wav
    * url: https://freesound.org/s/109904/
    * license: Attribution
  * 109903__Corsica_S__neptune.wav
    * url: https://freesound.org/s/109903/
    * license: Attribution
  * 109902__Corsica_S__nail.wav
    * url: https://freesound.org/s/109902/
    * license: Attribution
  * 109901__Corsica_S__mushroom.wav
    * url: https://freesound.org/s/109901/
    * license: Attribution
  * 109900__Corsica_S__mouse.wav
    * url: https://freesound.org/s/109900/
    * license: Attribution
  * 109899__Corsica_S__mars.wav
    * url: https://freesound.org/s/109899/
    * license: Attribution
  * 109898__Corsica_S__lion.wav
    * url: https://freesound.org/s/109898/
    * license: Attribution
  * 109897__Corsica_S__lemon.wav
    * url: https://freesound.org/s/109897/
    * license: Attribution
  * 109896__Corsica_S__leaf.wav
    * url: https://freesound.org/s/109896/
    * license: Attribution
  * 109895__Corsica_S__king.wav
    * url: https://freesound.org/s/109895/
    * license: Attribution
  * 109894__Corsica_S__key.wav
    * url: https://freesound.org/s/109894/
    * license: Attribution
  * 109893__Corsica_S__kayak.wav
    * url: https://freesound.org/s/109893/
    * license: Attribution
  * 109892__Corsica_S__juice.wav
    * url: https://freesound.org/s/109892/
    * license: Attribution
  * 109890__Corsica_S__jaguar.wav
    * url: https://freesound.org/s/109890/
    * license: Attribution
  * 109889__Corsica_S__jacket.wav
    * url: https://freesound.org/s/109889/
    * license: Attribution
  * 109888__Corsica_S__iron.wav
    * url: https://freesound.org/s/109888/
    * license: Attribution
  * 109887__Corsica_S__igloo.wav
    * url: https://freesound.org/s/109887/
    * license: Attribution
  * 109886__Corsica_S__ice_cream.wav
    * url: https://freesound.org/s/109886/
    * license: Attribution
  * 109885__Corsica_S__horse.wav
    * url: https://freesound.org/s/109885/
    * license: Attribution
  * 109884__Corsica_S__helicopter.wav
    * url: https://freesound.org/s/109884/
    * license: Attribution
  * 109883__Corsica_S__hammer.wav
    * url: https://freesound.org/s/109883/
    * license: Attribution
  * 109882__Corsica_S__glove.wav
    * url: https://freesound.org/s/109882/
    * license: Attribution
  * 109881__Corsica_S__glass.wav
    * url: https://freesound.org/s/109881/
    * license: Attribution
  * 109880__Corsica_S__galaxy.wav
    * url: https://freesound.org/s/109880/
    * license: Attribution
  * 109879__Corsica_S__frog.wav
    * url: https://freesound.org/s/109879/
    * license: Attribution
  * 109878__Corsica_S__fork.wav
    * url: https://freesound.org/s/109878/
    * license: Attribution
  * 109877__Corsica_S__fire.wav
    * url: https://freesound.org/s/109877/
    * license: Attribution
  * 109876__Corsica_S__elephant.wav
    * url: https://freesound.org/s/109876/
    * license: Attribution
  * 109875__Corsica_S__egg.wav
    * url: https://freesound.org/s/109875/
    * license: Attribution
  * 109874__Corsica_S__earth.wav
    * url: https://freesound.org/s/109874/
    * license: Attribution
  * 109873__Corsica_S__duck.wav
    * url: https://freesound.org/s/109873/
    * license: Attribution
  * 109872__Corsica_S__dog.wav
    * url: https://freesound.org/s/109872/
    * license: Attribution
  * 109871__Corsica_S__daisy.wav
    * url: https://freesound.org/s/109871/
    * license: Attribution
  * 109870__Corsica_S__cow.wav
    * url: https://freesound.org/s/109870/
    * license: Attribution
  * 109869__Corsica_S__car.wav
    * url: https://freesound.org/s/109869/
    * license: Attribution
  * 109868__Corsica_S__camera.wav
    * url: https://freesound.org/s/109868/
    * license: Attribution
  * 109867__Corsica_S__butterfly.wav
    * url: https://freesound.org/s/109867/
    * license: Attribution
  * 109866__Corsica_S__boat.wav
    * url: https://freesound.org/s/109866/
    * license: Attribution
  * 109865__Corsica_S__bear.wav
    * url: https://freesound.org/s/109865/
    * license: Attribution
  * 109864__Corsica_S__apple.wav
    * url: https://freesound.org/s/109864/
    * license: Attribution
  * 109863__Corsica_S__ant.wav
    * url: https://freesound.org/s/109863/
    * license: Attribution
  * 109862__Corsica_S__aeroplane.wav
    * url: https://freesound.org/s/109862/
    * license: Attribution


